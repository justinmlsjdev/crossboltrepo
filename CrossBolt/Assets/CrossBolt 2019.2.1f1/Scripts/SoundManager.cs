﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(AudioListener))]
public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    [SerializeField] AudioSource musicSource = null;
    [SerializeField] AudioSource audioSource = null;

    [Header("SFX")]
    public AudioClip swish;
    public AudioClip death;

    private void Awake()
    {
        if (instance) { Destroy(this); }
        instance = this;

        audioSource = GetComponent<AudioSource>();
        
        if (!musicSource)
        {
            musicSource = GetComponent<AudioSource>();
            if (!musicSource)
            {
                musicSource = gameObject.AddComponent<AudioSource>();
            }
        }
    }

    public void PlayOneShot(AudioClip audioClip)
    {
        if (!audioClip) { Debug.Log("Missing Audio Clip"); return; }
        if (!audioSource) { Debug.Log("Missing Audio Source"); return; }
        audioSource.PlayOneShot(audioClip);
    }

    public void ChangeMusic(AudioClip audioClip)
    {
        if (!audioClip) { Debug.Log("Missing Audio Clip"); return; }
        if (!musicSource) { Debug.Log("Missing Music Source"); return; }
        musicSource.clip = audioClip;
        musicSource.Play();
    }

}
