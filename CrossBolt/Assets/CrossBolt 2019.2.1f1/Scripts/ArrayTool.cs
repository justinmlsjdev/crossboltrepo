﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ArrayTool : SerializedMonoBehaviour
{

    public GridBoardGenerator grid;

    [Button]
    public void SetEntireBoard(int value)
    {
        for (int x = 0; x < grid.boardLayout.GetLength(0); x++)
        {
            for (int y = 0; y < grid.boardLayout.GetLength(1); y++)
            {
                grid.boardLayout[x, y] = value;
            }
        }
    }

    [Button]
    public void SetEntireColumn(int column, int value)
    {
        for (int y = 0; y < grid.boardLayout.GetLength(1); y++)
        {
            grid.boardLayout[column, y] = value;
        }
    }
    [Button]
    public void SetEntireRow(int row, int value)
    {
        for (int x = 0; x < grid.boardLayout.GetLength(0); x++)
        {
            grid.boardLayout[x, row] = value;
        }
    }
}
