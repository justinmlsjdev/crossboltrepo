﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Directions {
    Up,
    Left,
    Down,
    Right }

public class Direction
{
    public static Vector2 GetDirectionVector2(Directions dir)
    {
        switch (dir)
        {
            case Directions.Up:
                return new Vector2(0, 1);
            case Directions.Left:
                return new Vector2(-1, 0);
            case Directions.Down:
                return new Vector2(0, -1);
            case Directions.Right:
                return new Vector2(1, 0);
            default:
                return new Vector2();
        }
    }

    public static Vector3 GetDirectionVector3(Directions dir)
    {
        switch (dir)
        {
            case Directions.Up:
                return new Vector3(0, 0, -1);
            case Directions.Left:
                return new Vector3(-1, 0, 0);
            case Directions.Down:
                return new Vector3(0, 0, 1);
            case Directions.Right:
                return new Vector3(1, 0, 0);
            default:
                return new Vector3();
        }
    }
}
