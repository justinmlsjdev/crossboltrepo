﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class GridBoardGenerator : SerializedMonoBehaviour
{
    [HideInInspector] public Node[,] graph;

    public MapScriptableObject map;

    [HideInInspector]
    public int[,] boardLayout;

    public Transform holder;

    public float tileSpace;

    private int xWidth;
    private int yLength;

    private void Awake()
    {
        GenerateBoard();
    }

    [Button(30)]
    public void GenerateBoard()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }

        holder = new GameObject("Board").transform;
        holder.parent = transform;

        boardLayout = map.mapLayout;

        xWidth = boardLayout.GetLength(0);
        yLength = boardLayout.GetLength(1);
        graph = new Node[xWidth, yLength];

        for (int x = 0; x < xWidth; x++)
        {
            for (int y = 0; y < yLength; y++)
            {
                graph[x, y] = new Node();
            }
        }

        GenerateTiles();
        GeneratePathFindingGraph();
        GenerateUnits();
        UpdateCamera();
    }



    // Create a 2d Node array which will have references to it's neighbors
    void GeneratePathFindingGraph()
    {
        for (int x = 0; x < xWidth; x++)
        {
            for (int y = 0; y < yLength; y++)
            {
                graph[x, y].x = x;
                graph[x, y].y = y;
            }
        }


        for (int x = 0; x < xWidth; x++)
        {
            for (int y = 0; y < yLength; y++)
            {
                if (x > 0)
                    graph[x, y].neighbors.Add(graph[x - 1, y]);
                if (x < xWidth - 1)
                    graph[x, y].neighbors.Add(graph[x + 1, y]);
                if (y > 0)
                    graph[x, y].neighbors.Add(graph[x, y - 1]);
                if (y < yLength - 1)
                    graph[x, y].neighbors.Add(graph[x, y + 1]);
            }
        }
    }


    // Create a board, if it has a player then add it to the GameManager player list, if it has an enemy then add it to the GameManager enemy list
    void GenerateTiles()
    {
        for (int x = 0; x < xWidth; x++)
        {
            for (int y = 0; y < yLength; y++)
            {
                // Create board
                GameObject instantiatedTile = Instantiate(map.tiles[boardLayout[x, y]], (new Vector3(x, 0, -y) * tileSpace), Quaternion.identity, holder);
                instantiatedTile.name = "Tile " + x + " " + y;

                TileScript tile = instantiatedTile.GetComponent<TileScript>();
                graph[x, y].tile = tile;
                tile.tileNode = graph[x, y];

                tile.SetTileGraphic((x + y) % 2 == 0);
            }
        }
    }

    private void GenerateUnits()
    {
        if (EnemyManager.instance)
        {
            EnemyManager.instance.ClearEnemies();
        }



        GameObject unitHolder = GameObject.Find("=== Characters ===");

        if (unitHolder != null)
        {
            DestroyImmediate(unitHolder);
        }

        unitHolder = new GameObject("=== Characters ===");

        for (int x = 0; x < map.units.Count; x++)
        {
            UnitBase unit = Instantiate(map.units[x].unitPrefab, unitHolder.transform).GetComponent<UnitBase>();
            Node startNode = graph[(int)map.units[x].spawnLocation.x, (int)map.units[x].spawnLocation.y];
            unit.transform.position = startNode.tile.transform.position;
            unit.tileNode = startNode;
            startNode.SetOccupant(unit);

            EnemyController enemyController = unit.GetComponent<EnemyController>();
            if (enemyController)
            {
                enemyController.coordinates = map.units[x].movement;
            }

        }
    }

    private void UpdateCamera()
    {
        AlignToCentre camAlign = Camera.main.GetComponent<AlignToCentre>();

        if (camAlign)
        {
            camAlign.holder = holder;
            camAlign.Align();
        }
    }
}
