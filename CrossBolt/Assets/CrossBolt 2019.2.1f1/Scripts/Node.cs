﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public List<Node> neighbors = new List<Node>();
    public UnitBase occupant;
    public TileScript tile;

    public int x;
    public int y;

    public Vector2 NodeCoord
    {
        get
        {
            return new Vector2(x, y);
        }
    }

    public float DistanceTo(Node n)
    {
        return Mathf.Abs (x - n.x) + Mathf.Abs (y - n.y);
    }

    public bool IsOccupied
    {
        get
        {
            return occupant != null;
        }
    }

    public void SetOccupant(UnitBase newOccupant)
    {
        occupant = newOccupant;
    }
}
