﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Map", menuName = "Map", order = 1)]
public class MapScriptableObject : SerializedScriptableObject
{
    [TableMatrix(HorizontalTitle = "X", VerticalTitle = "Y", ResizableColumns = true)]
    public int[,] mapLayout;

    public GameObject[] tiles;
    public List<EnemySpawn> units;

    public int actionPoints;

    [Button(30, ButtonStyle.FoldoutButton)]
    public void SetEntireBoard(int value)
    {
        for (int x = 0; x < mapLayout.GetLength(0); x++)
        {
            for (int y = 0; y < mapLayout.GetLength(1); y++)
            {
                mapLayout[x, y] = value;
            }
        }
    }

    [Button(30, ButtonStyle.FoldoutButton)]
    public void SetEntireColumn(int column, int value)
    {
        for (int y = 0; y < mapLayout.GetLength(1); y++)
        {
            mapLayout[column, y] = value;
        }
    }

    [Button(30, ButtonStyle.FoldoutButton)]
    public void SetEntireRow(int row, int value)
    {
        for (int x = 0; x < mapLayout.GetLength(0); x++)
        {
            mapLayout[x, row] = value;
        }
    }
}

[System.Serializable]
public class EnemySpawn
{
    public GameObject unitPrefab;
    public Vector2 spawnLocation;
    public List<Vector2> movement;
}