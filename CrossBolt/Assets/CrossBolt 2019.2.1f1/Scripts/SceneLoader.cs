﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{

    private const string path = "Prefabs/UI/LoadingScreen";

    public float fadeTime = 0.2f;

    private Transform parent;

    private void Awake()
    {
        parent = GameObject.Find("=== UI ===").GetComponent<RectTransform>();
    }



    public void LoadScene(int index)
    {
        StartCoroutine(LoadAsynchronously(index));
    }

    public void ResetScene()
    {
        StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().buildIndex));
    }

    IEnumerator LoadAsynchronously (int scene)
    {
        GameObject loadingScreen = Resources.Load(path) as GameObject;
        GameObject instance = Instantiate(loadingScreen);
        instance.transform.SetParent(parent, false);

        Slider loadingBar = instance.GetComponentInChildren<Slider>();
        Image fader = instance.GetComponentInChildren<Image>();

        Color faderColor = fader.color;

        float progress = 0;
        float elasped = 0;

        while (elasped < fadeTime)
        {
            faderColor.a = (elasped / fadeTime);
            fader.color = faderColor;

            elasped += Time.deltaTime;
            yield return null;
        }

        yield return null;

        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        while (!operation.isDone)
        {
            progress = Mathf.Clamp01(operation.progress / .9f);

            loadingBar.value = progress;

            yield return null;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
