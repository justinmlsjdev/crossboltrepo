﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIHolder : MonoBehaviour
{
    public static UIHolder instance;

    public TextMeshProUGUI availableActions;

    private void Awake()
    {
        if (instance) { Destroy(this); }
        instance = this;
    }
}
