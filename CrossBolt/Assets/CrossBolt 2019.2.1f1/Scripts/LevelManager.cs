﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public GridBoardGenerator gridGenerator = null;

    public Node[,] grid
    {
        get
        {
            return gridGenerator.graph;
        }
    }
    public int xWidth
    {
        get
        {
            return grid.GetLength(0);
        }
    }
    public int yLength
    {
        get
        {
            return grid.GetLength(1);
        }
    }

    public float gridTileSpacing
    {
        get
        {
            return gridGenerator.tileSpace;
        }
    }

    public Node GetNode(int xCoord, int yCoord)
    {
        return grid[xCoord, yCoord];
    }
    public Node GetNode(Vector2 coords)
    {
        return grid[(int)coords.x, (int)coords.y];
    }

    void Awake()
    {
        if (instance) { Destroy(this); }
        instance = this;
    }
}
