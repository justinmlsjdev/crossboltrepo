﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathMover
{

    public const float stepDuration = 0.05f;

    public static void MoveUnitTo(UnitBase unit, Vector2 destination)
    {
        unit.StartCoroutine(PathMove(unit, destination));
    }


    public static IEnumerator PathMove(UnitBase unit, Vector2 destination)
    {
        //Returns the fastest path from unit to destination FindPath A* Algorithm Found Online
        List<Node> path = PathFinder.FindPath(unit.unitCoord, destination, LevelManager.instance.grid);

        bool run = true;
        bool couldMove = true;
        int step = 0;

        while (unit.actionPoints > 0 && step < path.Count && run)
        {
            // [Occupied] || [Non-Moveable] then stop moving
            if (!IsTileMoveable(path[step]) || path[step].IsOccupied)
            {
                couldMove = false;
                run = false;
            }
            else
            {
                couldMove = true;
                SwitchTile(unit, path[step].NodeCoord, stepDuration);
            }

            yield return new WaitForSeconds(stepDuration);
            step++;
        }

        if (couldMove)
        {
            unit.ActionComplete();
        }
        else
        {
            unit.ActionCanceled();
        }
    }

    public static void SwitchTile(UnitBase unit, Vector2 newPosition, float duration)
    {
        if (unit.unitCoord != newPosition)
        {
            TileScript newTile = LevelManager.instance.GetNode(newPosition).tile;
            Node oldNode = unit.tileNode;

            // Get off the old tile
            unit.tileNode.SetOccupant(null);

            // Get on the new tile
            newTile.tileNode.SetOccupant(unit);
            unit.tileNode = newTile.tileNode;

            // Move the object
            ObjectMove.MoveObject(unit.transform, newTile.transform.position, duration);

            // Set Unit Facing direction
            if (newTile.tileNode.x > oldNode.x)
            {
                unit.ChangeFacingDirection(Directions.Right);
            }
            else if(newTile.tileNode.x < oldNode.x)
            {
                unit.ChangeFacingDirection(Directions.Left);
            }
            else if (newTile.tileNode.y < oldNode.y)
            {
                unit.ChangeFacingDirection(Directions.Down);
            }
            else
            {
                unit.ChangeFacingDirection(Directions.Up);
            }
        }
    }

    public static bool IsTileExists(Vector2 coords)
    {
        int xWidth = LevelManager.instance.grid.GetLength(0);
        int yLength = LevelManager.instance.grid.GetLength(1);

        if (coords.x < 0)
            return false;
        if (coords.x > xWidth - 1)
            return false;
        if (coords.y < 0)
            return false;
        if (coords.y > yLength - 1)
            return false;

        if (LevelManager.instance.GetNode(coords).Equals(null))
            return false;

        return true;
    }

    public static bool IsTileMoveable(Node node)
    {
        return node.tile.attributes.HasFlag(Attributes.Moveable);
    }

    public static bool IsTileOccupied(Node node)
    {
        return node.IsOccupied;
    }
}
