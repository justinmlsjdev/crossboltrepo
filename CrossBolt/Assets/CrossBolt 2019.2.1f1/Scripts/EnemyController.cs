﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitBase))]
public class EnemyController : MonoBehaviour
{
    private UnitBase unit;

    public List<Vector2> coordinates;

    public int index;

    private void Awake()
    {
        unit = GetComponent<UnitBase>();
        unit.onAction += Unit_onAction;
        unit.onDeath += Unit_onDeath;
    }
    private void Start()
    {
        EnemyManager.instance.AddEnemy(this);
    }

    private void Unit_onDeath()
    {
        EnemyManager.instance.RemoveEnemy(this);
        WaveManager.instance.GenerateWave(transform.position);
        SoundManager.instance.PlayOneShot(SoundManager.instance.death);
    }

    private void Unit_onAction()
    {
        if (unit.unitCoord == coordinates[index])
        {
            index = (index + 1) % coordinates.Count;
            unit.AddActionPoints(1);
        }
    }

    public void MoveToCurrentDestination()
    {
        if (coordinates.Count > 0)
        {
            if (PathMover.IsTileExists(coordinates[index]))
            {
                PathMover.MoveUnitTo(unit, coordinates[index]);
            }
        }
    }
}
