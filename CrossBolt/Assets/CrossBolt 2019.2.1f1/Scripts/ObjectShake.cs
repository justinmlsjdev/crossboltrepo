﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ObjectShake
{

    public static void ShakePosition(Transform obj, Vector3 origin, float duration = 0.3f, float magnitude = 0.05f, float settleTime = 0f)
    {
        if (EnumeratorHolder.instance == null)
        {
            EnumeratorHolder.instance = new GameObject("Enum Holder").AddComponent<EnumeratorHolder>();
        }

        EnumeratorHolder.instance.StartCoroutine(Shake(obj, origin, duration, magnitude, settleTime));
    }

    public static void ShakePunch(Transform obj, Vector3 origin, float duration = 0.3f, float magnitude = 0.05f, float settleTime = 0f)
    {
        if (EnumeratorHolder.instance == null)
        {
            EnumeratorHolder.instance = new GameObject("Enum Holder").AddComponent<EnumeratorHolder>();
        }

        EnumeratorHolder.instance.StartCoroutine(Punch(obj, origin, duration, magnitude));
    }

    public static IEnumerator Shake(Transform obj, Vector3 origin, float duration = 0.3f, float magnitude = 0.05f, float settleTime = 0.1f)
    {
        Vector3 originalPos = origin;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            obj.localPosition = originalPos + new Vector3(x, y, 0);

            elapsed += Time.deltaTime;

            yield return null;
        }

        elapsed = 0.0f;

        while (elapsed < settleTime)
        {
            obj.localPosition = Vector3.Lerp(obj.localPosition, originalPos, elapsed / settleTime);

            elapsed += Time.deltaTime;
            yield return null;
        }

        obj.localPosition = originalPos;
    }

    public static IEnumerator Punch(Transform obj, Vector3 origin, float duration = 0.3f, float magnitude = 0.05f)
    {
        Vector3 originalSize = origin;
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            obj.localScale = originalSize + (Vector3.one * (1 - (elapsed / duration)));

            elapsed += Time.deltaTime;
            yield return null;
        }

        obj.localScale = originalSize;
    }
}
