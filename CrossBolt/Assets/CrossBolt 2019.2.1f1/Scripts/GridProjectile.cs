﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridProjectile : MonoBehaviour
{

    public GameObject projectile;

    [SerializeField] float projectileSpeed = 0.001f;

    [SerializeField] float pierceDelay = 0.05f;
    [SerializeField] float pierceComboIncrease = 0.05f;

    [SerializeField] float shakeAmount = 0.1f;
    [SerializeField] float shakeComboIncrease = 0.02f;

    [SerializeField] float shakeTime = 0.1f;
    [SerializeField] float shakeTimeIncrease = 0.1f;

    public void ShootBolt(UnitBase unit, Directions dir)
    {
        if (!projectile)
        {
            return;
        }
        StartCoroutine(Bolt(unit, dir, projectileSpeed));
    }

    IEnumerator Bolt(UnitBase unit, Directions dir, float speed)
    {
        List<Node> tileUntilWall = PathFinder.UntilWall(unit.tileNode, dir);

        Vector2 dirAsVector = Direction.GetDirectionVector2(dir);
        Vector3 newDir = new Vector3(dirAsVector.x, 0, -dirAsVector.y);

        float tileSpacing = LevelManager.instance.gridTileSpacing;
        int dist = tileUntilWall.Count;
        int step = 0;

        int enemiesHit = 0;

        if (tileUntilWall.Count <= 0)
        {
            unit.ActionCanceled();
            yield break;
        }

        SoundManager.instance.PlayOneShot(SoundManager.instance.swish);

        GameObject instantiatedBolt = Instantiate(projectile, unit.transform.position, Quaternion.LookRotation(newDir, Vector3.up));
        TrailRenderer trail = instantiatedBolt.GetComponentInChildren<TrailRenderer>();
        trail.time = 1 * tileUntilWall.Count;

        while (step < tileUntilWall.Count)
        {
        
            ObjectMove.MoveObject(instantiatedBolt.transform, instantiatedBolt.transform.position + instantiatedBolt.transform.forward * (tileSpacing), speed);
            yield return new WaitForSeconds(speed);

            if (tileUntilWall[step].IsOccupied)
            {
                enemiesHit++;

                ObjectShake.ShakePosition(Camera.main.transform, Camera.main.transform.position, shakeTime + (shakeTimeIncrease * enemiesHit), shakeAmount + (shakeComboIncrease * enemiesHit));
                yield return new WaitForSeconds(pierceDelay + (pierceComboIncrease * enemiesHit));
                tileUntilWall[step].occupant.Death();
            }

            step += 1;
        }
        ObjectMove.MoveObject(instantiatedBolt.transform, instantiatedBolt.transform.position + instantiatedBolt.transform.forward * 0.5f, 0.5f * speed);

        yield return new WaitForSeconds(0.5f * speed);
        trail.time = 0.2f;

        unit.ActionComplete();
    }
}
