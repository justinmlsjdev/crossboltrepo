﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance;

    public List<EnemyController> enemies = new List<EnemyController>();

    private void Awake()
    {
        if (instance) { Destroy(this); }
        instance = this;
    }

    public void AddEnemy(EnemyController enemy)
    {
        enemies.Add(enemy);
    }

    public void RemoveEnemy(EnemyController enemy)
    {
        enemies.Remove(enemy);
    }

    public void EnemyMove()
    {
        for (int x = 0; x < enemies.Count; x++)
        {
            enemies[x].MoveToCurrentDestination();
        }
    }

    public void ClearEnemies()
    {
        enemies.Clear();
    }
}
