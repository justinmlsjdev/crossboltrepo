﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class FunctionTest : MonoBehaviour
{

    public List<UnityEvent> OnClick = new List<UnityEvent>();

}

#if UNITY_EDITOR
[CustomEditor(typeof(FunctionTest))]
public class ObjectBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        FunctionTest myScript = (FunctionTest)target;

        if (GUILayout.Button("Add Element"))
        {
            myScript.OnClick.Add(new UnityEvent());
        }
        if (GUILayout.Button("Remove Element"))
        {
            if (myScript.OnClick.Count > 0)
            {
                myScript.OnClick.Remove(myScript.OnClick[myScript.OnClick.Count - 1]);
            }
        }
        if (GUILayout.Button("Clear Elements"))
        {
            myScript.OnClick = new List<UnityEvent>();
        }

        DrawDefaultInspector();

        if (myScript.OnClick.Count == 0)
        {
            return;
        }

        for (int x = 0; x < myScript.OnClick.Count; x++)
        {
            if (GUILayout.Button("Element " + x))
            {
                myScript.OnClick[x].Invoke();
            }
        }
    }
}
#endif
