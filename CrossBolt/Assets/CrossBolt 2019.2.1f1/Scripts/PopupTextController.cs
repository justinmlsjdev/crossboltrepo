﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class PopupTextController : MonoBehaviour
{
    private const int X = 0;

    //static methods require static references
    private static GameObject popupText;
    private static RectTransform parent;

    public const string popupDamage = "Prefabs/UI/PopupDamage";
    public const string popupHealth = "Prefabs/UI/PopupHealth";

    public static GameObject CreatePopup(string popupUI, Transform location)
    {
        GameObject instance = CreateAtPosition(popupUI, location.transform);
        return instance;
    }

    public static GameObject CreateAtPosition(string popupUI, Transform location, Vector3 offset = default)
    {
        popupText = Resources.Load(popupUI) as GameObject;

        parent = GameObject.Find("=== UI ===").GetComponent<RectTransform>();

        GameObject instance = Instantiate(popupText);

        Vector2 screenPos = Camera.main.WorldToScreenPoint(location.position + offset);
        instance.transform.SetParent(parent, false);
        instance.transform.position = screenPos;

        return instance;
    }
}