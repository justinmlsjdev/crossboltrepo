﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeDelay : MonoBehaviour
{

    public static void DelayInvoke(DelegateVoid delayedFunction, float delay)
    {
        if (EnumeratorHolder.instance == null)
        {
            EnumeratorHolder.instance = new GameObject("Enum Holder").AddComponent<EnumeratorHolder>();
        }

        EnumeratorHolder.instance.StartCoroutine(WaitInvoke(delayedFunction, delay));
    }

    public static IEnumerator WaitInvoke(DelegateVoid delayedFunction, float delay)
    {
        yield return new WaitForSeconds(delay);
        delayedFunction?.Invoke();
    }
}
