﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumeratorHolder : MonoBehaviour
{

    public static EnumeratorHolder instance;

    private void Awake()
    {
        if (instance) { Destroy(this); }
        instance = this;
    }
}
