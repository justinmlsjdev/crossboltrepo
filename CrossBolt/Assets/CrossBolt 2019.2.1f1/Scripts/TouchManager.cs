﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour
{
    void Update()
    {
        //foreach (Touch touch in Input.touches)
        //{
        //    if (touch.phase == TouchPhase.Began)
        //    {

        //        RaycastHit hit;

        //        // Construct a ray from the current touch coordinates
        //        Ray ray = Camera.main.ScreenPointToRay(touch.position);

        //        if (Physics.Raycast(ray, out hit))
        //        {
        //            Debug.Log(hit.collider.name);
        //        }
        //    }
        //}
    }

    // Used if Orthographic and tiles have no colliders
    Vector2 RoundToNearestTilePosition(Vector3 pos)
    {
        float xMultiple = Mathf.Round(pos.x / 1.2f);
        float yMultiple = -Mathf.Round(pos.z / 1.2f);

        // xMultiple *= 1.2f;
        // yMultiple *= 1.2f;
        return new Vector2(xMultiple, yMultiple);
    }
}
