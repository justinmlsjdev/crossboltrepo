﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void DelegateVoid();

public class UnitBase : MonoBehaviour
{
    public Node tileNode;

    public event DelegateVoid onAction;
    public event DelegateVoid onActCancel;
    public event DelegateVoid onDeath;

    public Directions currentFacingDirection;

    public Vector2 startCoords = default;
    public Vector2 unitCoord
    {
        get
        {
            return tileNode.NodeCoord;
        }
    }

    public int actionPoints = 3;

    public void ActionComplete()
    {
        onAction?.Invoke();
    }

    public void ActionCanceled()
    {
        onActCancel?.Invoke();
    }

    public void AddActionPoints(int amount)
    {
        actionPoints += amount;
    }

    public void Death()
    {
        tileNode.SetOccupant(null);
        onDeath?.Invoke();
        Destroy(gameObject);
    }

    public void ChangeFacingDirection(Directions dir)
    {
        currentFacingDirection = dir;
        transform.forward = Direction.GetDirectionVector3(currentFacingDirection);
    }
}
