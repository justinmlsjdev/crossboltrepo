﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove
{
    public static void MoveObject(Transform obj, Vector3 destination, float duration = 0.3f)
    {
        if (EnumeratorHolder.instance == null)
        {
            EnumeratorHolder.instance = new GameObject("Enum Holder").AddComponent<EnumeratorHolder>();
        }

        EnumeratorHolder.instance.StartCoroutine(Move(obj, destination, duration));
    }

    public static IEnumerator Move(Transform obj, Vector3 destination, float duration = 0.3f)
    {
        Vector3 startPos = obj.position;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            obj.position = Vector3.Lerp(startPos, destination, elapsed / duration);

            elapsed += Time.deltaTime;
            yield return null;
        }

        obj.position = destination;
    }
}
