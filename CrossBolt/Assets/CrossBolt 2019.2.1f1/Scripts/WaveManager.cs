﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;

    [SerializeField] private AnimationCurve wave = null;

    [SerializeField] private float waveSpeed = 5;
    [SerializeField] private float magnitude = 1;
    [SerializeField] private float duration = 1;

    private void Awake()
    {
        if (instance) { Destroy(this); }
        instance = this;
    }

    [Button(30)]
    public void GenerateWave(Vector3 point = default)
    {
        for (int x = 0; x < LevelManager.instance.xWidth; x++)
        {
            for (int y = 0; y < LevelManager.instance.yLength; y++)
            {
                TileScript tile = LevelManager.instance.grid[x, y].tile;
                float distance = Vector3.Distance(point, LevelManager.instance.grid[x, y].tile.transform.position);

                InvokeDelay.DelayInvoke(() => { tile.StopAllCoroutines(); tile.StartCoroutine(Wave(tile.transform)); }, distance / waveSpeed);
            }
        }
    }

    IEnumerator Wave(Transform waveItem)
    {
        Vector3 startPos = new Vector3(waveItem.position.x, 0, waveItem.position.z);

        float elapsed = 0;

        while (elapsed < duration)
        {
            waveItem.position = startPos + (Vector3.up * wave.Evaluate(elapsed / duration)) * magnitude;

            elapsed += Time.deltaTime;
            yield return null;
        }

        waveItem.position = startPos;
        yield break;
    }
}
