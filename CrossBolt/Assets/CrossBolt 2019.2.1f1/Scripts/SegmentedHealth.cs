﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentedHealth : MonoBehaviour
{

    public Transform holder;

    public GameObject segmentedHealthPrefab;

    public List<GameObject> healthbars;

    const float totalHealthSize = 100f;

    const float spacing = 5f;

    void Awake()
    {
        holder = transform.GetChild(0);
    }

    public void InitializeHealthBar(int maxHealth, int health)
    {
        for (int x = 0; x < maxHealth; x++)
        {
            GameObject bar = Instantiate(segmentedHealthPrefab, holder.transform.position, Quaternion.identity, holder);
            RectTransform barTransform = bar.GetComponent<RectTransform>();
            barTransform.localPosition = Vector3.zero;

            float segmentSize = totalHealthSize / maxHealth;

            barTransform.sizeDelta = new Vector3(segmentSize - (spacing / 2), barTransform.sizeDelta.y);

            bar.transform.localPosition += Vector3.right * -((segmentSize * (maxHealth - 1)) / 2) + Vector3.right * (x * segmentSize);
            healthbars.Add(bar);
        }
    }

    public void UpdateHealth(int health)
    {
        for (int x = 0; x < healthbars.Count; x++)
        {
            healthbars[x].SetActive(x < health);
        }
    }
}
