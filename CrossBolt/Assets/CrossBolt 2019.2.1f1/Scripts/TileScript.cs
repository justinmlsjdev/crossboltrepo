﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum Attributes
{
    Moveable = 0x1,
    Wall = 0x2,
    Slow = 0x3
}

public class TileScript : MonoBehaviour
{
    public Node tileNode;

    public float moveCost = 0;

    public Attributes attributes;

    public GameObject tilePositive, tileNegative;

    [SerializeField] bool usePositiveNegative = true;

    public void SetTileGraphic(bool positive)
    {
        if (!tileNegative || !tilePositive)
        {
            return;
        }

        if (!usePositiveNegative)
        {
            tilePositive.SetActive(true);
            DestroyImmediate(tileNegative);
            return;
        }

        tilePositive.SetActive(positive);
        tileNegative.SetActive(!positive);

        if (positive)
        {
            DestroyImmediate(tileNegative);
        }
        else
        {
            DestroyImmediate(tilePositive);
        }
    }
}
