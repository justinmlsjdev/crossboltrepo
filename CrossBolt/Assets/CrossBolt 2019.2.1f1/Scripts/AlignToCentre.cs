﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class AlignToCentre : SerializedMonoBehaviour
{
    [SerializeField] private Vector3 offset = new Vector3();
    public Transform holder = null;

    [SerializeField] bool AllowAllign = true;

    [Button]
    public void Align()
    {
        if (!AllowAllign) { return; }
        Vector3 centre = new Vector3();

        if (holder)
        {
            for (int x = 0; x < holder.childCount; x++)
            {
                centre += holder.GetChild(x).position;
            }

            centre /= holder.childCount;
        }

        transform.position = centre + offset;
    }
}
