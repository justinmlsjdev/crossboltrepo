﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEvents : MonoBehaviour
{

    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }


    public void TurnOffAnimator()
    {
        Destroy(anim);
        Destroy(this);
    }

    public void RandomPauseAnim()
    {
        StartCoroutine(PauseAnimation(Random.Range(0.00f, 0.80f)));
    }

    IEnumerator PauseAnimation(float duration)
    {
        float elapsed = 0;
        anim.speed = 0;

        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            yield return null;
        }

        anim.speed = 1;
    }
}
