﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitBase))]
public class PlayerController : MonoBehaviour
{
    public UnitBase unit = null;

    public bool actionInProgress = false;

    public GridProjectile weapon;

    private void Awake()
    {
        unit = GetComponent<UnitBase>();
        unit.onAction += Unit_onAction;
        unit.onActCancel += Unit_onActCancel;
    }

    private void Start()
    {
        unit.actionPoints = LevelManager.instance.gridGenerator.map.actionPoints;
        UIHolder.instance.availableActions.text = "Actions: " + unit.actionPoints.ToString();
    }

    private void Unit_onActCancel()
    {
        actionInProgress = false;
    }

    private void Unit_onAction()
    {
        actionInProgress = false;
        unit.AddActionPoints(-1);
        UIHolder.instance.availableActions.text = "Actions: " + unit.actionPoints.ToString();
        EnemyManager.instance.EnemyMove();
    }

    private void Update()
    {
        InputCheck(KeyCode.W, Directions.Down);
        InputCheck(KeyCode.A, Directions.Left);
        InputCheck(KeyCode.S, Directions.Up);
        InputCheck(KeyCode.D, Directions.Right);

        AttackInputCheck(KeyCode.UpArrow, Directions.Down);
        AttackInputCheck(KeyCode.LeftArrow, Directions.Left);
        AttackInputCheck(KeyCode.DownArrow, Directions.Up);
        AttackInputCheck(KeyCode.RightArrow, Directions.Right);
    }

    private void InputCheck(KeyCode key, Directions dir)
    {
        if (ActionNotAvailable())
        {
            return;
        }

        if (Input.GetKeyDown(key))
        {
            Vector2 direction = Direction.GetDirectionVector2(dir);

            Vector2 tileCoord = unit.unitCoord + direction;

            if (PathMover.IsTileExists(tileCoord))
            {
                PathMover.MoveUnitTo(unit, tileCoord);
                actionInProgress = true;
            }
        }
    }

    private void AttackInputCheck(KeyCode key, Directions dir)
    {
        if (ActionNotAvailable())
        {
            return;
        }

        if (Input.GetKeyDown(key))
        {
            actionInProgress = true;
            weapon.ShootBolt(unit, dir);
            unit.ChangeFacingDirection(dir);
        }
    }

    private bool ActionNotAvailable()
    {
        return (actionInProgress || unit.actionPoints <= 0);
    }
}
