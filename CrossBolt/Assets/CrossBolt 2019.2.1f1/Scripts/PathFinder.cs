﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{

    // ================ FindPath Code Found Online as a generic A* Alogrithm ===================
    public static List<Node> FindPath(Vector2 start, Vector2 destination, Node[,] graph)
    {
        Dictionary<Node, float> dist = new Dictionary<Node, float>();
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

        List<Node> unvisited = new List<Node>();

        Node source = graph[(int)start.x, (int)start.y];
        Node target = graph[(int)destination.x, (int)destination.y];

        dist[source] = 0;
        prev[source] = null;


        // initialize everything to have infinite distance, since 
        // we don't know if some nodes can't be reached
        foreach (Node index in graph)
        {
            if (index != source)
            {
                dist[index] = Mathf.Infinity;
                prev[index] = null;
            }
            unvisited.Add(index);
        }

        while (unvisited.Count > 0)
        {
            // u is node with shortest distance to target
            Node u = null;
            foreach (Node possibleU in unvisited)
            {
                if (u == null || dist[possibleU] < dist[u])
                {
                    u = possibleU;
                }
            }

            if (u == target)
            {
                // We have our path stop loop
                break;
            }

            unvisited.Remove(u);

            foreach (Node index in u.neighbors)
            {
                // float alt = dist[u] + u.DistanceTo(v);
                float alt = dist[u] + CostToEnterTile(index) + index.DistanceTo(target);

                if (alt < dist[index])
                {
                    dist[index] = alt;
                    prev[index] = u;
                }
            }
        }
        // Shortest route found or no route found
        if (prev[target] == null)
        {
            // No route found to target from source
        }

        List<Node> currentPath = new List<Node>();
        Node curr = target;

        // Step through the "Prev" chain and add it to path
        while (prev[curr] != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }

        // Right now the path describes our route from our target to our source

        currentPath.Reverse();

        return currentPath;
    }

    private static float CostToEnterTile(Node tileNode)
    {
        if (tileNode.IsOccupied)
        {
            return 1000;
        }
        else
        {
            return tileNode.tile.moveCost;
        }
    }

    public static List<Node> UntilWall(Node currentNode, Directions dir)
    {
        List<Node> untilWall = new List<Node>();

        Vector2 direction = Direction.GetDirectionVector2(dir);
        Vector2 currentCoord = currentNode.NodeCoord;

        bool stillSpace = true;


        while (stillSpace)
        {
            if (PathMover.IsTileExists(currentCoord + direction))
            {
                Node newTileNode = LevelManager.instance.GetNode(currentCoord + direction);

                if (newTileNode.tile.attributes.HasFlag(Attributes.Wall))
                {
                    stillSpace = false;
                    return untilWall;
                }

                untilWall.Add(LevelManager.instance.GetNode(currentCoord + direction));

                currentCoord += direction;
            }
            else
            {
                stillSpace = false;
            }
        }
        return untilWall;
    }
}
